# frozen_string_literal: true

require 'net/http'

module ReleaseTools
  module Deployments
    class BlockerAnnotations
      include ::SemanticLogger::Loggable

      def initialize
        @annotated_blockers = []
      end

      def execute
        return unless Feature.enabled?(:grafana_annotations)

        deployment_blockers.each do |blocker|
          if blocker.need_to_annotate?
            annotated_blockers << blocker
          else
            logger.info("Skipping annotation for blocker: #{blocker.data.web_url}")
          end
        end

        add_grafana_annotations
      end

      private

      attr_accessor :annotated_blockers

      def deployment_blockers
        fetcher = ReleaseTools::Deployments::BlockerIssueFetcher.new
        fetcher.fetch

        fetcher.deployment_blockers
      end

      def add_grafana_annotations
        annotated_blockers.each do |blocker|
          ReleaseTools::Deployments::GrafanaAnnotator
            .new(blocker)
            .execute
        end
      end
    end
  end
end
