# frozen_string_literal: true

module ReleaseTools
  module ParallelMethods
    # Use twice the amount of processors because usually the parallelized work
    # is IO bound (waiting for network, for example: GitLab API).
    DEFAULT_THREAD_COUNT = Etc.nprocessors * 2

    def parallel_each(items, in_threads: thread_count, &)
      Parallel.each(items, in_threads: in_threads, &)
    end

    def parallel_map(items, in_threads: thread_count, &)
      Parallel.map(items, in_threads: in_threads, &)
    end

    private

    def thread_count
      Integer(ENV.fetch('PARALLEL_PROCESSOR_COUNT', DEFAULT_THREAD_COUNT))
    end
  end
end
