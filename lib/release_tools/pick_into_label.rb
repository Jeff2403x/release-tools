# frozen_string_literal: true

module ReleaseTools
  class PickIntoLabel
    include ::SemanticLogger::Loggable

    GROUP = 'gitlab-org'
    COLOR = '#00C8CA'
    DESCRIPTION = 'Merge requests to cherry-pick into the `%s` branch.'

    def self.escaped(version)
      CGI.escape(self.for(version))
    end

    def self.for(version)
      if version == :auto_deploy
        "Pick into auto-deploy"
      else
        "Pick into #{version.to_minor}"
      end
    end

    def self.reference(version)
      %[~"#{self.for(version)}"]
    end

    def initialize(version)
      @version = version
      @label = self.class.for(@version)
    end
  end
end
