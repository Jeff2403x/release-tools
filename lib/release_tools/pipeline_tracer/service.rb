# frozen_string_literal: true

module ReleaseTools
  module PipelineTracer
    # Entry point for the PipelineTracer module
    class Service
      include ::SemanticLogger::Loggable

      attr_reader :pipeline, :pipeline_name, :tracer, :trace_depth

      def self.from_pipeline_url(pipeline_url, pipeline_name: nil, trace_depth: 2)
        pipeline = Pipeline.from_url(pipeline_url)

        new(
          pipeline: pipeline,
          pipeline_name: pipeline_name || pipeline_url,
          trace_depth: trace_depth
        )
      end

      # @param [PipelineTracer::Pipeline] pipeline
      # @param [string] pipeline_name is the name that should be given to the generated trace.
      # @param [integer] trace_depth can be any integer >= 0. It is the depth to which this pipeline should be traced.
      #         For example,
      #         if trace_depth is 0, the given pipeline will not be traced. There will only be a single span
      #         representing the entire pipeline.
      #         If trace_depth is 1, the given pipeline will be traced, and will have spans representing
      #         each job, but downstream pipelines triggered by this pipeline will not be traced.
      #         If trace_depth is 2, downstream pipelines will also be traced, so there will be spans for individual
      #         jobs in the downstream pipeline, but if the downstream pipelines also trigger more downstream
      #         pipelines, those 2nd level downstream pipelines will not be traced.
      # @return [PipelineTracer::Service]
      def initialize(pipeline:, pipeline_name:, trace_depth: 2)
        @pipeline = pipeline
        @pipeline_name = pipeline_name
        @trace_depth = trace_depth
      end

      def execute
        logger.info('Tracing pipeline', web_url: pipeline.url)

        return if SharedStatus.dry_run?

        @tracer = ::OpenTelemetry.tracer_provider.tracer

        process_pipeline
      end

      private

      def process_pipeline
        # TODO (rpereira2): Do not attempt to generate traces for any pipeline that has not completed:
        # https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20040.
        # To make this work, the traces job should run in a child pipeline that the parent pipeline does not wait for:
        # https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/20039.
        unless pipeline.end_time
          logger.info('Not generating traces for pipeline since end_time is unknown', pipeline_url: pipeline.url)
          return
        end

        root_span = tracer.start_span(
          pipeline_name,
          kind: :internal,
          start_timestamp: Time.parse(pipeline.start_time),
          attributes: pipeline.root_attributes.stringify_keys
        )

        if trace_depth >= 1
          generate_sub_spans(root_span)
        else
          logger.info('trace_depth is 0, not generating sub-spans', pipeline_url: pipeline.url)
        end

        root_span.finish(end_timestamp: Time.parse(pipeline.end_time))
      end

      def generate_sub_spans(root_span)
        OpenTelemetry::Trace.with_span(root_span) do |_span, _context|
          ProcessJobs.new(tracer, pipeline, trace_depth: trace_depth).execute

          process_pipeline_bridges
        end
      end

      def process_pipeline_bridges
        pipeline.bridge_jobs.each_page do |page|
          page.each do |bridge|
            next unless bridge.downstream_pipeline

            self.class
              .from_pipeline_url(
                bridge.downstream_pipeline.web_url,
                pipeline_name: "#{bridge.name} bridged downstream pipeline",
                trace_depth: trace_depth - 1
              )
              .execute
          end
        end
      end
    end
  end
end
