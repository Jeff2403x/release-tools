# frozen_string_literal: true

module ReleaseTools
  module Pre
    class Prepare
      include ::SemanticLogger::Loggable

      def initialize
        @deployment = last_deployment_on_gprd
      end

      def execute
        version = ProductVersion.from_metadata_sha(deployment.sha)

        logger.info('Storing latest auto-deploy package running on production', version: version.to_s)

        deploy_version = version.auto_deploy_package

        File.write('build.env', "DEPLOY_VERSION=#{deploy_version}")
      end

      private

      attr_reader :deployment

      def last_deployment_on_gprd
        Retriable.with_context(:api) do
          client.last_successful_deployment(
            project.ops_path,
            'db/gprd'
          )
        end
      end

      def client
        ReleaseTools::GitlabOpsClient
      end

      def project
        ReleaseTools::Project::Release::Metadata
      end
    end
  end
end
