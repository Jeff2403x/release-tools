# frozen_string_literal: true

module ReleaseTools
  module Project
    class CellsTissue < BaseProject
      REMOTES = {
        ops: 'git@ops.gitlab.net:gitlab-com/delivery/cells-tissue.git'
      }.freeze

      def self.default_branch
        'main'
      end

      # NOTE: without canonical defined .path is an endless loop
      def self.path
        ops_path
      end
    end
  end
end
