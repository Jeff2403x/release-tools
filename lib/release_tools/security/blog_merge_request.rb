# frozen_string_literal: true

module ReleaseTools
  module Security
    class BlogMergeRequest < MergeRequest
      include ::SemanticLogger::Loggable

      def title
        self[:title] || 'Draft: Security blog post'
      end

      def description
        self[:description] || ''
      end
    end
  end
end
