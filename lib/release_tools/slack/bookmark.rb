# frozen_string_literal: true

module ReleaseTools
  module Slack
    class Bookmark
      include ::SemanticLogger::Loggable

      EDIT_URL = 'https://slack.com/api/bookmarks.edit'
      LIST_URL = 'https://slack.com/api/bookmarks.list'

      NoCredentialsError = Class.new(StandardError)
      SlackBookmarkError = Class.new(StandardError)

      def self.list(channel:)
        raise NoCredentialsError unless slack_token.present?

        post(LIST_URL, { channel_id: channel })
      end

      def self.edit(channel:, bookmark:, link:, title: nil)
        raise NoCredentialsError unless slack_token.present?

        params = {
          channel_id: channel,
          bookmark_id: bookmark
        }

        params[:link] = link if link
        params[:title] = title if title

        logger.trace(__method__, params)

        return {} if SharedStatus.dry_run?

        if Feature.enabled?(:slack_down)
          logger.warn("Not attempting to call Slack API because FF 'slack_down' is enabled", params: params)
          return {}
        end

        post(EDIT_URL, params)
      end

      def self.post(url, params)
        response = client.post(url, json: params)

        unless response.status.success?
          logger.warn('Slack API call was not successful', code: response.code, response_body: response.body, request_params: params)
          raise SlackBookmarkError.new("#{response.code} #{response.reason} #{response.body}")
        end

        body = JSON.parse(response.body)
        # The slack API will return 200 OK for errors with body['ok'] = false and body['error'] with a message
        # similar to graphql error handling
        return body if body['ok']

        logger.warn('Slack API call returned error', response_body: body, request_params: params)
        raise SlackBookmarkError.new("#{response.code} #{response.reason} #{body}")
      end

      class << self
        private

        def client
          @client ||= HTTP.auth("Bearer #{slack_token}")
        end

        def slack_token
          ENV.fetch('SLACK_APP_BOT_TOKEN', '')
        end
      end
    end
  end
end
