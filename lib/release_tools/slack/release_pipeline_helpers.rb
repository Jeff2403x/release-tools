# frozen_string_literal: true

module ReleaseTools
  module Slack
    module ReleasePipelineHelpers
      EMOJI = {
        patch: ":security-tanuki:",
        monthly: ":release:"
      }.freeze
    end
  end
end
