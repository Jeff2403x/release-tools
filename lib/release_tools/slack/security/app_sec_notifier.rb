# frozen_string_literal: true

module ReleaseTools
  module Slack
    module Security
      class AppSecNotifier
        include ::SemanticLogger::Loggable
        include Utilities

        def initialize(blog_post_url, tracking_issue_url)
          @blog_post_url = blog_post_url
          @tracking_issue_url = tracking_issue_url
        end

        def send_blog_post_notification
          logger.info('Posting a message in the AppSec slack channel')

          ReleaseTools::Slack::Message.post(
            channel: ReleaseTools::Slack::SEC_APPSEC,
            message: fallback_message,
            blocks: slack_blocks
          )
        end

        private

        attr_reader :blog_post_url, :tracking_issue_url

        def fallback_message
          "The patch blog post has been generated: #{blog_post_url}"
        end

        def slack_blocks
          blocks = ::Slack::BlockKit.blocks

          blocks.section { |section| section.mrkdwn(text: section_block) }
          blocks.context { |context| context.append(clock_context_element) }
          blocks.as_json
        end

        def section_block
          [].tap do |text|
            text << ':security-tanuki:'
            text << ':ci_passing:'
            text << "*The <#{blog_post_url}|blog post> for the <#{tracking_issue_url}|upcoming patch release> has been successfully created.*"
          end.join(' ')
        end
      end
    end
  end
end
