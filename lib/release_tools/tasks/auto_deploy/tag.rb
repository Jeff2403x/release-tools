# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module AutoDeploy
      class Tag
        include ::SemanticLogger::Loggable

        def initialize
          @branch = ReleaseTools::AutoDeployBranch.current
          @version = ReleaseTools::AutoDeploy::Tag.current
          @metadata = ReleaseTools::ReleaseMetadata.new
        end

        def execute
          logger.info('Preparing for tagging', branch: branch.to_s, version: version)

          find_passing_build

          omnibus_builder = ReleaseTools::AutoDeploy::Builder::Omnibus.new(branch, commit.id, metadata)
          cng_builder = ReleaseTools::AutoDeploy::Builder::CNGImage.new(branch, commit.id, metadata)

          if omnibus_builder.changes? || cng_builder.changes?
            logger.info('Tagging a new product version')

            omnibus_builder.execute
            cng_builder.execute
            upload_metadata

            # this triggers the package rollout in a coordinator pipeline
            tag_coordinator
          else
            logger.info('Nothing to tag on Omnibus or CNG')
          end
        end

        private

        attr_reader :branch, :version, :metadata

        def find_passing_build
          logger.info('Searching for commit with passing build', branch: branch.to_s)
          commit
        end

        def tag_coordinator
          ReleaseTools::AutoDeploy::Tagger::Coordinator.new.tag!
        end

        def upload_metadata
          ReleaseTools::ReleaseMetadataUploader.new.upload(version, metadata, auto_deploy: true)
        end

        def commit
          @commit ||= ReleaseTools::PassingBuild.new(branch.to_s).for_auto_deploy_tag
        end
      end
    end
  end
end
