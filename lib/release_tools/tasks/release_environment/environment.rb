# frozen_string_literal: true

module ReleaseTools
  module Tasks
    module ReleaseEnvironment
      class Environment
        include ReleaseTools::Tasks::Helper

        attr_reader :version

        def initialize(version)
          @version = version
        end

        def execute
          ReleaseTools::ReleaseEnvironment::Environment
          .new(version)
          .create
        end
      end
    end
  end
end
