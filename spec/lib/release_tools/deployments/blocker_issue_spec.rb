# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ReleaseTools::Deployments::BlockerIssue do
  let(:labels) do
    [
      'Deploys-blocked-gprd::4hr',
      'Deploys-blocked-gstg::0.5hr',
      'incident',
      'Incident::Resolved',
      'Service::Kube',
      'Source::IMA::IncidentDeclare',
      'severity::3',
      'RootCause::Software-Change'
    ]
  end

  let(:issue) do
    create(
      :issue,
      title: 'incident blocker',
      labels: labels,
      created_at: "2023-12-05T04:29:15.929Z"
    )
  end

  subject(:blocker) do
    described_class.new(issue)
  end

  describe '#title' do
    it 'returns an expandable web_url' do
      expect(blocker.title).to eq("#{issue.web_url}+")
    end
  end

  describe '#hours_gstg_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gstg label' do
        expect(blocker.hours_gstg_blocked).to eq(0.5)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end

  describe '#hours_gprd_blocked' do
    context 'with deploy block label' do
      it 'fetches the hours from the gprd label' do
        expect(blocker.hours_gprd_blocked).to eq(4)
      end
    end

    context 'with no deploy block label' do
      let(:labels) { ['incident'] }

      it { expect(blocker.hours_gstg_blocked).to eq(0) }
    end
  end

  describe '#root_cause' do
    context 'when the labels include a root cause' do
      it 'returns the root cause label' do
        expect(blocker.root_cause).to eq("~\"RootCause::Software-Change\"")
      end
    end

    context 'when the labels do not include a root cause label' do
      let(:labels) do
        [
          'Deploys-blocked-gprd::4hr',
          'Deploys-blocked-gstg::0.5hr',
          'incident',
          'Incident::Resolved',
          'severity::3'
        ]
      end

      it 'returns nothing' do
        expect(blocker.root_cause).to be_nil
      end
    end
  end

  describe '#root_cause_label' do
    it 'returns the root cause label' do
      expect(blocker.root_cause_label).to eq("RootCause::Software-Change")
    end

    context 'when the labels do not include a root cause label' do
      let(:labels) do
        [
          'Deploys-blocked-gprd::4hr',
          'Deploys-blocked-gstg::0.5hr',
          'incident',
          'Incident::Resolved',
          'severity::3'
        ]
      end

      it 'returns nothing' do
        expect(blocker.root_cause_label).to be_nil
      end
    end
  end

  describe '#created_at' do
    it 'returns the created_at time' do
      expect(blocker.created_at).to eq(issue.created_at)
    end
  end

  describe '#ended_at' do
    it 'returns the end time based on Deploys-blocked labels' do
      expect(blocker.ended_at).to eq(Time.parse("2023-12-05T08:29:15.929Z"))
    end
  end

  describe '#need_to_annotate' do
    it 'when the blocker is not flaky and lasts longer than 30 minutes' do
      expect(blocker.need_to_annotate?).to be(true)
    end

    context 'when the blocker is a flaky test and lasts 30min' do
      let(:labels) do
        [
          'Deploys-blocked-gstg::0.5hr',
          'RootCause::Flaky-Test'
        ]
      end

      it 'return false' do
        expect(blocker.need_to_annotate?).to be(false)
      end
    end
  end
end
