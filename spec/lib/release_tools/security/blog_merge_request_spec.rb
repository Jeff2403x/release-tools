# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::BlogMergeRequest do
  let(:params) { { title: 'Blog post', description: 'Description' } }

  subject(:merge_request) { described_class.new(params) }

  describe '#title' do
    it 'returns given title' do
      expect(subject.title).to eq('Blog post')
    end

    context 'when no title given' do
      let(:params) { {} }

      it 'returns default title' do
        expect(subject.title).to eq('Draft: Security blog post')
      end
    end
  end

  describe '#description' do
    it 'returns given description' do
      expect(subject.description).to eq('Description')
    end

    context 'when no description given' do
      let(:params) { {} }

      it 'returns empty description' do
        expect(subject.description).to eq('')
      end
    end
  end
end
