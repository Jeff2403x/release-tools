# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Security::MergeRequestHelper do
  before do
    foo_class = Class.new do
      include ReleaseTools::Security::MergeRequestHelper
    end

    stub_const('FooClass', foo_class)
  end

  let(:foo_class) { FooClass.new }
  let(:client) { stub_const('ReleaseTools::GitlabClient', spy) }

  describe '#security_blog_merge_request' do
    it 'fetches the security blog request' do
      expect(client).to receive(:security_blog_merge_request)

      foo_class.security_blog_merge_request
    end
  end

  describe '#canonical_blog_merge_request' do
    it 'fetches the canonical blog request' do
      expect(client).to receive(:security_blog_merge_request).with(security: false)

      foo_class.canonical_blog_merge_request
    end
  end
end
