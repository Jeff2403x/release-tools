# frozen_string_literal: true

require 'rake_helper'

describe 'security tasks', :rake do
  after do
    # rake tasks in the security namespace run the force_security task altering
    # the global state
    ENV.delete('SECURITY')
  end

  describe 'sync_git_tags', task: 'security:sync_git_tags' do
    it 'syncs git tags' do
      expect(ReleaseTools::Security::SyncGitRemotesService).to receive(:new)
        .with(['1.0', '2.0', 'v3.1'])
        .and_return(instance_double(ReleaseTools::Security::SyncGitRemotesService, execute: true))

      task.invoke('1.0 2.0 v3.1')
    end
  end

  describe 'prepare:review_security_fixes', task: 'security:prepare:review_security_fixes' do
    it 'verifies the fixes included in the patch release' do
      expect(ReleaseTools::Security::Prepare::FixesVerifier).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Prepare::FixesVerifier, execute: true))

      task.invoke
    end
  end

  describe 'publish:move_blog_post', task: 'security:publish:move_blog_post' do
    it 'moves the blog post to the handbook canonical repo' do
      expect(ReleaseTools::Security::Publish::MoveBlogPost).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Publish::MoveBlogPost, execute: true))

      task.invoke
    end
  end

  describe 'publish:deploy_blog_post', task: 'security:publish:deploy_blog_post' do
    it 'publishes the patch release blog post' do
      expect(ReleaseTools::Security::Publish::DeployBlogPost).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Publish::DeployBlogPost, execute: true))

      task.invoke
    end
  end

  describe 'finalize:enable_security_target_processor', task: 'security:finalize:enable_security_target_processor' do
    it 'enables the security target processor' do
      expect(ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::Finalize::ToggleSecurityTargetProcessor, execute: true))

      task.invoke
    end
  end

  describe 'process_security_target_issues', task: 'security:process_security_target_issues' do
    it 'runs the processor' do
      expect(ReleaseTools::Security::TargetIssuesProcessor).to receive(:new)
        .and_return(instance_double(ReleaseTools::Security::TargetIssuesProcessor, execute: true))

      task.invoke
    end
  end

  describe 'finalize:create_release_status_metric', task: 'security:finalize:create_release_status_metric' do
    it 'creates a new patch release status metric with open status' do
      expect(ReleaseTools::Metrics::PatchReleaseStatus).to receive(:new).with(status: :open)
        .and_return(instance_double(ReleaseTools::Metrics::PatchReleaseStatus, execute: true))

      task.invoke
    end
  end
end
